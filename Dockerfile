FROM ubuntu:18.04

RUN apt-get update && apt-get -y install git wget unzip curl python mysql-client jq ca-certificates

RUN wget https://download.docker.com/linux/static/edge/x86_64/docker-19.03.1.tgz && \
    tar xvfz docker-19.03.1.tgz && \
    mv ./docker/docker /usr/local/bin

RUN wget https://releases.hashicorp.com/terraform/0.12.20/terraform_0.12.20_linux_amd64.zip && \
   unzip terraform_0.12.20_linux_amd64.zip && \
   mv terraform /usr/local/bin

RUN curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip" && \
   unzip awscli-bundle.zip && \
   ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
   
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - && \
   apt-get -y install nodejs

RUN apt-get clean &&  apt-get purge && rm -rf /var/lib/apt/lists/*

CMD [init.sh]
